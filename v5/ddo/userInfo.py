from pydantic import BaseModel
from typing import Optional


class UserData(BaseModel):
    firstName: str
    lastName: str


class UserInfo(BaseModel):
    user: Optional[UserData]