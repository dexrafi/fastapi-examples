from fastapi import APIRouter
from ddo.userInfo import UserInfo
router = APIRouter()


@router.get("")
async def get_user_data(id: str) -> UserInfo:
    return {"user":  {"firstName": "John", "lastName": "Kowalsky"}}
