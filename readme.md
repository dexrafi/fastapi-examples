# The FAST API examples project 

## Pre requirements
1. to start from scratch fastapi project you need install only `pip install fastapi`
2. in these examples i used also `pip install pydantic` (for data models) and `pip install uvicorn` as server

## examples
all examples please run by command: `uvicorn main:app --reload` inside all examples folders (v1,v2...)

### example v1
This is minimal one file example nothing really live example,
please verify the OpenAPI auto-doc on http://localhost:8000/docs or by the ReDoc on http://localhost:8000/docs   

### example v2
This examples shows how to add raw additional POST action (on the same url) 

### example v3
This examples hows how to divide into modules application written using FastAPI
 
### example v4
In this example i added DTO object, please check also swagger returned values (FastAPI added automatically 422
response for validation errors)

### example v5
In this example I defined the response schema format to show in documentation how the returned model looks like 
(using DTO model) 

### example v6
Added async dependency mechanism to simulate authorization check or db connection in async way, in main file added 
example function to show wss communication
 
### example v7
Added ws connection management and extended wss example to implement multi user chat 

## Pros and Cons

Pros of Fast API:
+ little overhead to implement business logic
+ fast and furious
+ support for asynchronous code
+ not too big dependencies 
+ easy  testing (not shown in examples)
+ easy deployment

Cons of FastAPI:
- need to use external modules for DB interoperation  
- no build-in singleton in Dependency Injection
- ugly request validation errors

## Summary 
FastAPI is designed to be as fast as possible to work with I/O bound operations 
(most of typical RestAPI cases), but not for  CPU bound (directly), normally user 
should create new worker for CPU band operations (publish subscriber patter is needed 
for scalable architecture). Fallowing this rules the best DB interfaces and libraries 
for FastAPI should utilize the asyncio await/async pattern. 

## Appendix 1 (Deployment notes)
You can easily deploy your FastAPI app via Docker using FastAPI provided docker image. 
You can also deploy it to AWS Lamdba using [Mangum](https://pypi.org/project/mangum/).

