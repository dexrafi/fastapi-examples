
from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


async def get_current_user():
    return "user"


async def get_current_active_user(current_user: str = Depends(get_current_user)):
    if False:
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user
