from fastapi import APIRouter

router = APIRouter()


@router.get("")
async def get_user_data():
    return {"user":  {"firstName": "John", "lastName": "Kowalsky"}}
