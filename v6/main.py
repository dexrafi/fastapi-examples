from fastapi import FastAPI, Depends, WebSocket
from routes import user
from utils.auth import get_current_active_user

app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Hello World"}


app.include_router(user.router, prefix="/auth", dependencies=[Depends(get_current_active_user)])


@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    while True:
        data = await websocket.receive_text()
        await websocket.send_text(f"Message text was: {data}")
